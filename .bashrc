#!/bin/bash
stty -ixon # Disable ctrl-s and ctrl-q.
shopt -s autocd #Allows you to cd into directory merely by typing the directory name.
HISTSIZE= HISTFILESIZE= # Infinite history.
source /usr/share/bash-completion/bash_completion
export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
if [ -f /usr/lib/bash-git-prompt/gitprompt.sh ]; then
	# To only show the git prompt in or under a repository directory
	GIT_PROMPT_ONLY_IN_REPO=1
	# To use upstream's default theme
	# GIT_PROMPT_THEME=Default
	# To use upstream's default theme, modified by arch maintainer
	#GIT_PROMPT_THEME=Default_Arch
	GIT_PROMPT_THEME=Single_line_Solarized
	source /usr/lib/bash-git-prompt/gitprompt.sh
fi

[ -f "$HOME/.config/shortcutrc" ] && source "$HOME/.config/shortcutrc" # Load shortcut aliases
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

eval "$(thefuck --alias)"
