
--vim.keymap.set('n', 'c', '\"_c')

-- Shortcutting split navigation, saving a keypress:
vim.keymap.set('', '<C-h>', '<C-w>h')
vim.keymap.set('', '<C-j>', '<C-w>j')
vim.keymap.set('', '<C-k>', '<C-w>k')
vim.keymap.set('', '<C-l>', '<C-w>l')

-- Spell-check set to <leader>s:
vim.keymap.set('n', '<leader>s', function()
	vim.o.spell = not vim.o.spell
	vim.o.spelllang = 'en_us'
	print('spell: ' .. tostring(vim.o.spell))
end)

-- ToggleProse
--" vimling:
--"nm <leader>d :call ToggleDeadKeys()<CR>
--"imap <leader>d <esc>:call ToggleDeadKeys()<CR>a
--"	nm <leader>i :call ToggleIPA()<CR>
--"	imap <leader>i <esc>:call ToggleIPA()<CR>a
--"nm <leader>q :call ToggleProse()<CR>
vim.keymap.set('n', '<leader>q', function()
		vim.api.nvim_call_function('ToggleProse', {})
end)

-- Diagnostic keymaps
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic message' })
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic message' })
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = 'Open floating diagnostic message' })
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = 'Open diagnostics list' })
