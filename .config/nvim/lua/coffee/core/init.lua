vim.g.mapleader = '\\'
vim.opt.langmenu = 'en_US.utf8'
vim.cmd.language('en_US.utf8')
vim.opt.go = 'a'
vim.opt.mouse = 'a'
vim.opt.hlsearch = false
vim.opt.clipboard = 'unnamedplus'
vim.opt.diffopt = 'filler,internal,algorithm:histogram,indent-heuristic'
vim.opt.tabstop = 4
vim.opt.termguicolors = true
vim.wo.signcolumn = 'yes'



vim.cmd.filetype('plugin', 'on')
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.path:append { '**' }
vim.opt.wildmode = 'longest,list,full'

-- Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
vim.opt.splitbelow = true
vim.opt.splitright = true

vim.g.do_filetype_lua = 1
vim.g.did_load_filetypes = 0

-- Backup files
vim.opt.backup = false    -- use backup files
vim.opt.writebackup = false
vim.opt.swapfile = false -- do not use swap file
local HOME = os.getenv('HOME')
vim.opt.undodir = HOME .. '/.vim/tmp/undo/'     -- undo files
vim.opt.backupdir = HOME .. '/.vim/tmp/backup/' -- backups
vim.opt.directory = HOME .. '/.vim/tmp/swap/'   -- swap files

-- Better display for messages
vim.o.cmdheight = 2

-- You will have bad experience for diagnostic messages when it's default 4000.
vim.go.updatetime = 300

-- don't give |ins-completion-menu| messages.
--vim.opt.shortmess:append { 'c' }

vim.opt.completeopt = 'menu,menuone,noselect'

-- Diagnositcs config
vim.fn.sign_define('DiagnosticSignError', { text='❌', texthl='DiagnosticSignError', linehl='', numhl=''})
vim.fn.sign_define('DiagnosticSignWarn', { text='‼', texthl='DiagnosticSignWarn', linehl='', numhl=''})
vim.fn.sign_define('DiagnosticSignInfo', { text='❕', texthl='DiagnosticSignInfo', linehl='', numhl=''})
vim.fn.sign_define('DiagnosticSignHint', { text='💡', texthl='DiagnosticSignHint', linehl='', numhl=''})
