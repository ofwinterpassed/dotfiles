return {
	{
		-- LSP Configuration & Plugins
		'neovim/nvim-lspconfig',
		dependencies = {
			-- Automatically install LSPs to stdpath for neovim
			'williamboman/mason.nvim',
			'williamboman/mason-lspconfig.nvim',

			-- Useful status updates for LSP
			-- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
			{ 'j-hui/fidget.nvim', tag = 'legacy', opts = {} },

			-- Additional lua configuration, makes nvim stuff amazing!
			'folke/neodev.nvim',
			'hrsh7th/cmp-nvim-lsp',
			'nvim-tree/nvim-web-devicons',
			--Please make sure you install markdown and markdown_inline parser
			'nvim-treesitter/nvim-treesitter',
			'p00f/clangd_extensions.nvim'
		},
		config = function()
			-- Setup lspconfig.
			local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol
				.make_client_capabilities())

			-- [[ Configure LSP ]]
			--  This function gets run when an LSP connects to a particular buffer.
			local on_attach = function(client, bufnr)
				-- NOTE: Remember that lua is a real programming language, and as such it is possible
				-- to define small helper and utility functions so you don't have to repeat yourself
				-- many times.
				--
				-- In this case, we create a function that lets us more easily define mappings specific
				-- for LSP related items. It sets the mode, buffer and description for us each time.
				local map = function(modes, keys, func, desc)
					if desc then
						desc = 'LSP: ' .. desc
					end

					vim.keymap.set(modes, keys, func,
						{ noremap = true, silent = false, buffer = bufnr, desc = desc })
				end

				map('n', '<leader>rn', vim.lsp.buf.rename, '[R]e[n]ame')
				map('n', '<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')

				map('n', 'gd', require('telescope.builtin').lsp_definitions, '[G]oto [D]efinition')
				map('n', 'gr', require('telescope.builtin').lsp_references, '[G]oto [R]eferences')
				map('n', 'gI', require('telescope.builtin').lsp_implementations,
					'[G]oto [I]mplementation')
				map('n', '<leader>D', require('telescope.builtin').lsp_type_definitions,
					'Type [D]efinition')
				map('n', '<leader>ds', require('telescope.builtin').lsp_document_symbols,
					'[D]ocument [S]ymbols')
				map('n', '<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols,
					'[W]orkspace [S]ymbols')

				-- See `:help K` for why this keymap
				map('n', 'K', vim.lsp.buf.hover, 'Hover Documentation')
				map('n', '<leader>K', vim.lsp.buf.signature_help, 'Signature Documentation')

				-- Lesser used LSP functionality
				map('n', 'gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')
				map('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, '[W]orkspace [A]dd Folder')
				map('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
				map('n', '<leader>wl', function()
					print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
				end, '[W]orkspace [L]ist Folders')

				-- Create a command `:Format` local to the LSP buffer
				vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
					vim.lsp.buf.format { async = true }
				end, { desc = 'Format current buffer with LSP' })
				map({ 'n', 'v' }, '<leader>f', function() vim.lsp.buf.format { async = true } end,
					'[F]ormat file')

				if client.resolved_capabilities.document_highlight then
					vim.api.nvim_create_autocmd('CursorHold', {
						callback = vim.lsp.buf.document_highlight,
						group = "lsp_document_highlight",
					})
					vim.api.nvim_create_autocmd('CursorHoldI', {
						callback = vim.lsp.buf.document_highlight,
						group = "lsp_document_highlight",
					})
					vim.api.nvim_create_autocmd('CursorMoved', {
						callback = vim.lsp.buf.clear_references,
						group = "lsp_document_highlight",
					})
				end
			end

			-- mason-lspconfig requires that these setup functions are called in this order
			-- before setting up the servers.
			require('mason').setup()
			require('mason-lspconfig').setup()

			-- Setup neovim lua configuration
			require("neodev").setup({
				library = { plugins = { "nvim-dap-ui" }, types = true },
			})

			local servers = {
				lua_ls = {
					Lua = {
						run = {
							-- Tell the language server which version of Lua you're using
							--	(most likely LuaJIT in the case of Neovim)
							version = 'LuaJIT',
							-- Setup your lua path
							path = vim.split(package.path, ';'),
						},
						diagnostics = {
							-- Get the language server to recognize the `vim` global
							globals = { 'vim' },
						},
						workspace = {
							checkThirdParty = false,
							library = {
								vim.env.VIMRUNTIME
							},
							completion = {
								callSnippet = 'replace'
							},
						},
					}
				},
				clangd = { cmd = { 'clangd', '-j=62', '--background-index', '--background-index-priority=normal', '--pch-storage=memory', '--log=error' } },

				rust_analyzer = {}
			}
			-- Ensure the servers above are installed
			local mason_lspconfig = require 'mason-lspconfig'

			mason_lspconfig.setup {
				ensure_installed = vim.tbl_keys(servers),
			}

			mason_lspconfig.setup_handlers {
				function(server_name)
					require('lspconfig')[server_name].setup {
						capabilities = capabilities,
						on_attach = on_attach,
						settings = servers[server_name],
						filetypes = (servers[server_name] or {}).filetypes,
					}
				end,
			}
		end,
		event = { 'BufReadPre', 'BufNewFile' },
	},

	-- clangd-extensions
	{
		'p00f/clangd_extensions.nvim',
		event = { 'BufReadPre', 'BufNewFile' },
	},

	-- cmp
	{
		'hrsh7th/nvim-cmp',
		dependencies = {
			'hrsh7th/cmp-vsnip', 'hrsh7th/vim-vsnip',
			'hrsh7th/cmp-nvim-lsp',
			'hrsh7th/cmp-buffer',
			'hrsh7th/cmp-path',
			'hrsh7th/cmp-nvim-lsp-signature-help',
		},
		config = function()
			local cmp = require('cmp')

			cmp.setup({
				snippet = {
					-- REQUIRED - you must specify a snippet engine
					expand = function(args)
						vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
						-- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
						-- require('snippy').expand_snippet(args.body) -- For `snippy` users.
						-- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
					end,
				},
				window = {
					-- completion = cmp.config.window.bordered(),
					-- documentation = cmp.config.window.bordered(),
				},
				mapping = cmp.mapping.preset.insert({
					['<C-b>'] = cmp.mapping.scroll_docs(-4),
					['<C-f>'] = cmp.mapping.scroll_docs(4),
					['<C-Space>'] = cmp.mapping.complete(),
					['<C-e>'] = cmp.mapping.abort(),
					['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
				}),
				sources = cmp.config.sources({
					{ name = 'nvim_lsp_signature_help' },
					{ name = 'nvim_lsp' },
					-- { name = 'vsnip' }, -- For vsnip users.
					-- { name = 'luasnip' }, -- For luasnip users.
					-- { name = 'ultisnips' }, -- For ultisnips users.
					-- { name = 'snippy' }, -- For snippy users.
				}, {
					{ name = 'buffer' },
				})
			})

			-- Set configuration for specific filetype.
			cmp.setup.filetype('gitcommit', {
				sources = cmp.config.sources({
					{ name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
				}, {
					{ name = 'buffer' },
				})
			})
		end,
		event = { 'BufReadPre', 'BufNewFile' },
	},
	{
		'hrsh7th/cmp-cmdline',
		dependencies = { 'hrsh7th/nvim-cmp' },
		config = function()
			local cmp = require('cmp')
			-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
			cmp.setup.cmdline('/', {
				mapping = cmp.mapping.preset.cmdline(),
				sources = {
					{ name = 'buffer' }
				}
			})

			-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
			cmp.setup.cmdline(':', {
				mapping = cmp.mapping.preset.cmdline(),
				sources = cmp.config.sources({
					{ name = 'path' }
				}, {
					{ name = 'cmdline' }
				})
			})
		end,
		event = { 'BufReadPre', 'BufNewFile' },
	},

	-- Rust specific
	{
		'simrat39/rust-tools.nvim',
		dependencies = { 'neovim/nvim-lspconfig', 'nvim-lua/plenary.nvim', }, -- 'mfussenegger/nvim-dap' },
		config = function()
			local rt = require("rust-tools")

			rt.setup({
				--server = {
				--on_attach = function(_, bufnr)
				--custom_lsp_attach(_)
				-- Hover actions
				--vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions,
				--{ buffer = bufnr })
				-- Code action groups
				--vim.keymap.set("n", "<Leader>a", rt.code_action_group.code_action_group,
				--{ buffer = bufnr })
				--end,
				--},
			})
		end,
		event = { 'BufReadPre', 'BufNewFile' },
	},
}
