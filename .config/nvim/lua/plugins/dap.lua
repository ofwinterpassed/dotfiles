---@param config {args?:string[]|fun():string[]?}
local function get_args(config)
	local args = type(config.args) == "function" and (config.args() or {}) or config.args or {}
	newconfig = vim.deepcopy(config)
	---@cast args string[]
	newconfig.args = function()
		local new_args = vim.fn.input("Run with args: ", table.concat(args, " ")) --[[@as string]]
		return vim.split(vim.fn.expand(new_args) --[[@as string]], " ")
	end
	return newconfig
end

return {
	'mfussenegger/nvim-dap',
	dependencies = {
		{
			'theHamsta/nvim-dap-virtual-text',
			config = true,
		},
		{
			'rcarriga/nvim-dap-ui',
			dependencies = { "nvim-neotest/nvim-nio",
			-- mason.nvim integration
				"jay-babu/mason-nvim-dap.nvim",
			},
			config = function()
			end,
		},
		-- mason.nvim integration
		{
			"jay-babu/mason-nvim-dap.nvim",
			dependencies = { "mason.nvim", config = true },
			cmd = { "DapInstall", "DapUninstall" },
			opts = {
				-- Makes a best effort to setup the various debuggers with
				-- reasonable debug configurations
				automatic_installation = true,

				-- You can provide additional configuration to the handlers,
				-- see mason-nvim-dap README for more information
				handlers = {},

				-- You'll need to check that you have the required things installed
				-- online, please don't ask me how to install them :)
				ensure_installed = {
					-- Update this to ensure that you have the debuggers for the langs you want
				},
			},
		},
	},
	-- stylua: ignore
	keys = {
		{ "<f10>",      function() require("dap").set_breakpoint(vim.fn.input('Breakpoint condition: ')) end, desc = "DAP: Breakpoint Condition" },
		{ "<f9>",       function() require("dap").toggle_breakpoint() end,                                    desc = "DAP: Toggle Breakpoint" },
		{ "<f4>",       function() require("dap").continue() end,                                             desc = "DAP: Continue" },
		{ "<leader>dC", function() require("dap").run_to_cursor() end,                                        desc = "DAP: Run to Cursor" },
		{ "<leader>dg", function() require("dap").goto_() end,                                                desc = "DAP: Go to line (no execute)" },
		{ "<f1>",       function() require("dap").step_into() end,                                            desc = "DAP: Step Into" },
		{ "<f7>",       function() require("dap").down() end,                                                 desc = "DAP: Down" },
		{ "<f8>",       function() require("dap").up() end,                                                   desc = "DAP: Up" },
		{ "<f5>",       function() require("dap").run_last() end,                                             desc = "DAP: Run Last" },
		{ "<f3>",       function() require("dap").step_out() end,                                             desc = "DAP: Step Out" },
		{ "<f2>",       function() require("dap").step_over() end,                                            desc = "DAP: Step Over" },
		{ "<leader>dp", function() require("dap").pause() end,                                                desc = "DAP: Pause" },
		{ "<leader>dr", function() require("dap").repl.toggle() end,                                          desc = "DAP: Toggle REPL" },
		{ "<leader>ds", function() require("dap").session() end,                                              desc = "DAP: Session" },
		{ "<leader>dt", function() require("dap").terminate() end,                                            desc = "DAP: Terminate" },
		{ "<leader>dw", function() require("dap.ui.widgets").hover() end,                                     desc = "DAP: Widgets" },
		{ "<leader>cs", function() require('dapui').close() end,                                              desc = "DAP: Close UI" },
	},

	config = function()
		vim.api.nvim_set_hl(0, 'DapBreakpoint', { ctermbg = 0, fg = '#993939', bg = '#31353f' })
		vim.api.nvim_set_hl(0, 'DapLogPoint', { ctermbg = 0, fg = '#61afef', bg = '#31353f' })
		vim.api.nvim_set_hl(0, 'DapStopped', { ctermbg = 0, fg = '#98c379', bg = '#31353f' })

		vim.fn.sign_define('DapBreakpoint',
			{ text = '', texthl = 'DapBreakpoint', linehl = 'DapBreakpoint', numhl = 'DapBreakpoint' })
		vim.fn.sign_define('DapBreakpointCondition',
			{ text = 'ﳁ', texthl = 'DapBreakpoint', linehl = 'DapBreakpoint', numhl = 'DapBreakpoint' })
		vim.fn.sign_define('DapBreakpointRejected',
			{ text = '', texthl = 'DapBreakpoint', linehl = 'DapBreakpoint', numhl = 'DapBreakpoint' })
		vim.fn.sign_define('DapLogPoint',
			{ text = '', texthl = 'DapLogPoint', linehl = 'DapLogPoint', numhl = 'DapLogPoint' })
		vim.fn.sign_define('DapStopped',
			{ text = '', texthl = 'DapStopped', linehl = 'DapStopped', numhl = 'DapStopped' })

		local dap, dapui = require("dap"), require("dapui")
		dapui.setup()
		dap.listeners.after.event_initialized["dapui_config"] = function()
			dapui.open()
		end
		-- dap.listeners.before.event_terminated["dapui_config"] = function()
		-- 	dapui.close()
		-- end
		-- dap.listeners.before.event_exited["dapui_config"] = function()
		-- 	dapui.close()
		-- end

		local mason_registry = require("mason-registry")
		mason_registry.refresh()
		local cpptools_root = mason_registry.get_package("cpptools"):get_install_path() .. "/extension/"
		local cpptools_path = cpptools_root .. "debugAdapters/bin/OpenDebugAD7.exe"
		dap.adapters.cppdbg = {
			id = 'cppdbg',
			type = 'executable',
			command = cpptools_path, -- adjust as needed, must be absolute path
			options = {
				detached = false
			},
		}
		-- local codelldb_root = mason_registry.get_package("codelldb"):get_install_path() .. "/extension/"
		-- local codelldb_path = codelldb_root .. "adapter/codelldb.exe"
		-- local liblldb_path = codelldb_root .. "lldb/lib/liblldb.lib"
		-- dap.adapters.codelldb = {
		-- 	type = 'server',
		-- 	port = "${port}",
		-- 	executable = {
		-- 		-- CHANGE THIS to your path!
		-- 		command = codelldb_path, --vim.fn.stdpath('data') .. '/mason/packages/codelldb/extension/adapter/codelldb.exe',
		-- 		args = { "--port", "${port}" },
		-- 		--args = { "--liblldb", liblldb_path, "--port", "${port}" },
		--
		-- 		-- On windows you may have to uncomment this:
		-- 		detached = false,
		-- 	}
		-- }
		--dap.adapters.lldb = {
		--	type = 'executable',
		--	command = 'c:/Program Files/LLVM/bin/lldb-vscode.exe', -- adjust as needed, must be absolute path
		--	name = 'lldb'
		--}
	end,
}
