return { { "folke/lazy.nvim", version = "*" },

	-- The rest
	{ 'LukeSmithxyz/vimling' },
	{ 'lervag/vimtex' },
}
