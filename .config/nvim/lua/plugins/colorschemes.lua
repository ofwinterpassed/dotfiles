return {
	'sainnhe/sonokai',
	priority = 1000,
	config = function()
		vim.g.sonokai_style = "atlantis"
		vim.g.sonokai_transparent_background = true
		vim.cmd.colorscheme('sonokai')
	end
}
