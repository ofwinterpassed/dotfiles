return {
	'nvim-telescope/telescope.nvim',
	dependencies = {
		'nvim-lua/popup.nvim',
		'nvim-lua/plenary.nvim',
		{
			'nvim-telescope/telescope-fzf-native.nvim',
			build =
			'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build',
			--enabled = vim.fn.executable('make') == 1,
			config = function()
				require('telescope').load_extension('fzf')
			end,
		},
		{
			'nvim-telescope/telescope-file-browser.nvim',
			--dependencies = { 'nvim-telescope/telescope.nvim' },
			config = function()
				require('telescope').load_extension('file_browser')
				vim.keymap.set('n', '<leader>O', '<cmd>Telescope file_browser<cr>')
			end
		},
	},
	opts = {
		defaults = {
			buffer_previewer_maker = function(filepath, bufnr, opts)
				-- Truncate the preview of oversized buffers.
				local previewers = require('telescope.previewers')
				local previewers_utils = require('telescope.previewers.utils')
				local max_size = 100000

				local cmd
				if string.find(vim.loop.os_uname().sysname, 'Windows') ~= nil then
					cmd = { 'powershell.exe', '-command', 'Get-Content',
						filepath, '-Head ', max_size }
				else
					cmd = { 'head', '-c', max_size, filepath }
				end

				opts = opts or {}
				filepath = vim.fn.expand(filepath)

				vim.loop.fs_stat(filepath, function(_, stat)
					if not stat then return end
					if stat.size > max_size then
						previewers_utils.job_maker(cmd, bufnr, opts)
					else
						previewers.buffer_previewer_maker(filepath, bufnr, opts)
					end
				end)
			end,
			prompt_prefix = '$ ',
			mappings = {
				i = {
					['<C-j>'] = {
						'move_selection_next',
						type = "action",
						opts = { nowait = true, silent = true }
					},
					['<C-k>'] = {
						'move_selection_previous',
						type = "action",
						opts = { nowait = true, silent = true }
					}
				}
			}
		},
		extensions = {
			file_browser = {
				-- use the 'ivy' theme if you want
				theme = 'ivy',
			}
		}
	},
	config = function()
		vim.keymap.set('n', '<leader>o', function() require('telescope.builtin').find_files {} end)
	end
}
