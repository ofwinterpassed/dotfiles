return {
	'mbbill/undotree',
	event = { 'BufReadPre', 'BufNewFile' },
	config = function()
		vim.keymap.set('n', '<leader>ut', vim.cmd.UndotreeToggle,
			{ noremap = true, silent = true, desc = '[U]ndotree [T]oggle' })
		vim.keymap.set('n', '<leader>uf', vim.cmd.UndotreeFocus,
			{ noremap = true, silent = true, desc = '[U]ndotree {F}ocus' })
	end,
}
