return {
	'ofwinterpassed/gtestrunner.nvim',
	dependencies = {
		'mfussenegger/nvim-dap',
		'nvim-telescope/telescope.nvim',
		'nvim-treesitter/nvim-treesitter'
	},
	config = function()
		local dap, gtestrunner = require('dap'), require('gtestrunner')
		dap.configurations.cpp = {
			{
				name = function()
					return 'gdb ' .. gtestrunner.current_executable
				end,
				type = 'cppdbg',
				request = 'launch',
				program = function()
					return gtestrunner.current_executable
				end,
				cwd = '${workspaceFolder}',
				terminal = 'integrated',
				runInTerminal = false,
				stopAtEntry = false,
				args = {},
				MIMode = 'gdb',
				MIDebuggerPath = 'C:/Users/jchristenson/scoop/apps/gdb/11.1/bin/gdb.exe',
			},
		}
		vim.keymap.set('n', "tr", function() gtestrunner.run_gtest_under_cursor() end,
			{ desc = "DAP: Debug gtest by cursor" })
		vim.keymap.set('n', "ts", function() gtestrunner.run_gtestsuite_under_cursor() end,
			{ desc = "DAP: Debug gtest suite by cursor" })
		vim.keymap.set('n', "<leader>te", function() gtestrunner.set_debug_executable() end,
			{ desc = "DAP: Set debug executable" })
	end
}
