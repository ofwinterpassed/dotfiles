return {
	name = 'frostbite',
	dependencies = { 'ofwinterpassed/gtestrunner.nvim', "nvim-lua/plenary.nvim" },
	url = 'https://gitlab.ea.com/jchristenson/frostbite.nvim',
	opts = {
		base_dap_config = {
			type = 'cppdbg',
			request = 'launch',
			cwd = '${workspaceFolder}',
			terminal = 'integrated',
			runInTerminal = true,
			stopAtEntry = false,
			MIMode = 'gdb',
			MIDebuggerPath = 'C:/tools/gdb/bin/gdb.exe',
		}
	}
}
