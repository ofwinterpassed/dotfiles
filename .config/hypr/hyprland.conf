#
# Please note not all available settings / options are set here.
# For a full list, see the wiki
#


# See https://wiki.hyprland.org/Configuring/Monitors/
monitor = , preferred, auto, 1


# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
exec-once = swww init & mako & blueman-tray & waybar

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# Some default env vars.
env = XCURSOR_SIZE, 16

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = us, se
    kb_variant =
    kb_model =
    kb_options = caps:swapescape, grp:shifts_toggle
    kb_rules =

    follow_mouse = 1

    touchpad {
        natural_scroll = yes
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 0
    gaps_out = 0
    border_size = 1
    col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg
    col.inactive_border = rgba(595959aa)

    layout = master
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 3

    blur {
        enabled = true
        size = 3
        passes = 1
    }

    drop_shadow = yes
    shadow_range = 4
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
}

animations {
    enabled = yes

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 4, myBezier
    animation = windowsOut, 1, 4, default, popin 80%
    animation = border, 1, 4, default
    animation = borderangle, 1, 4, default
    animation = fade, 1, 4, default
    animation = workspaces, 1, 4, default
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = yes # you probably want this
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
	mfact = 0.65
    new_is_master = true
    new_on_top = true
	no_gaps_when_only = 1
	orientation = left
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = on
}
# Example per-device config
# See https://wiki.hyprland.org/Configuring/Keywords/#executing for more
device {
    name = epic-mouse-v1
    sensitivity = -0.5
}

# Example windowrule v1
# windowrule = float, ^(kitty)$
# Example windowrule v2
# windowrulev2 = float, class:^(kitty)$, title:^(kitty)$
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more


# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER
# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod SHIFT, Return, exec, alacritty
bind = $mainMod SHIFT, C, killactive,
bind = $mainMod SHIFT, Q, exit,
bind = $mainMod, E, exec, nautilus
bind = $mainMod SHIFT, space, togglefloating,
bind = $mainMod, P, exec, dmenu-wl_run
#bind = $mainMod, P, pseudo, # dwindle
bind = $mainMod, J, togglesplit, # dwindle
bind = $mainMod, Return, layoutmsg, swapwithmaster master
bind = $mainMod, f, fullscreen, 0
bind = $mainMod SHIFT, f, fakefullscreen, 0

# Move focus with mainMod + vim keys
bind = $mainMod, h, movefocus, l
bind = $mainMod, l, movefocus, r
bind = $mainMod, k, layoutmsg, cycleprev
bind = $mainMod, j, layoutmsg, cyclenext

# Move focus with mainMod + arrow keys
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
bind = $mainMod SHIFT, 0, movetoworkspace, 10

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

# Other bindings
bind = $mainMod, grave, exec, ~/.local/bin/dmenuunicode

bind = , xf86audiomute, exec, pactl set-sink-mute @DEFAULT_SINK@ toggle
binde = , xf86audiolowervolume, exec, pactl set-sink-volume @DEFAULT_SINK@ -5%
binde = , xf86audioraisevolume, exec, pactl set-sink-volume @DEFAULT_SINK@ +5%
binde = SHIFT, xf86audiolowervolume, exec, pactl set-sink-volume @DEFAULT_SINK@ -1%
binde = SHIFT, xf86audioraisevolume, exec, pactl set-sink-volume @DEFAULT_SINK@ +1%
binde = , xf86monbrightnessdown, exec, brighnessctl s 5%-
binde = , xf86monbrightnessup, exec, brighnessctl s +5%
binde = SHIFT, xf86monbrightnessdown, exec, brighnessctl s 1%-
binde = SHIFT, xf86monbrightnessup, exec, brighnessctl s +1%

# screensharing
exec-once = dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP

# volume notifications
exec-once = pactl subscribe | grep --line-buffered "sink" | while read -r UNUSED_LINE; do ~/.local/bin/volumenotify; done > /dev/null 2>&1


# exit mode
bind = SUPER, escape, exec, hyprctl dispatch submap logout; notify-send -p -a Hyprland $'\ne - exit\n\nr - reboot\n\ns - suspend\n\nS - poweroff\n\nl - lock' -i /usr/share/icons/breeze-dark/actions/32/system-suspend.svg > ~/.endnoteid
submap = logout
bindr = , e, exec, ~/.config/hyprland/exit.sh &
bindr = , s, exec, hyprctl dispatch submap reset && makoctl dismiss -n $(cat ~/.endnoteid) && swaylock && systemctl suspend
bindr = , r, exec, systemctl reboot
bindr = SHIFT, s, exec, systemctl poweroff -i
bindr = , l, exec, hyprctl dispatch submap reset && makoctl dismiss -n $(cat ~/.endnoteid) && swaylock
bindr = , escape, exec, makoctl dismiss -n $(cat ~/.endnoteid)
bindr = , escape, submap, reset
bind = , return, exec, makoctl dismiss -n $(cat ~/.endnoteid)
bind = , return, submap, reset
submap = reset
